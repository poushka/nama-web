<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Options extends Model
{


    //
    protected $table="options";
    public $timestamps=false;
    protected $primaryKey = 'option_id';

    protected $fillable = [
        'title','eng','price_type','required','type_id','active'
    ];



       public function type() {
           return $this->hasOne(Type::class,'type_id','type_id')->select('type_id','type');
       }

      public function optionvalues() {
          return $this->hasMany(Option_Value::class,'option_id');
      }





}
