<?php


namespace App\helper;


class TimeHelperClass
{
       static function getTime($created_at) {
           $result =  time() - $created_at->timestamp;
          if ($result<=60) {
              return "لحظاتی پیش";
          }else if ($result>60 && $result<=3600) {
              return "دقایقی پیش";
          }else if ($result>3600 && $result<=86400) {
              return "ساعاتی پیش";
          }else if ($result>86400 && $result<=172800) {
              return "یک روز پیش";
          }else if ($result>172800 && $result<=259200) {
              return "2 روز پیش";
          }else if ($result>259200 && $result<=345600) {
              return "3 روز پیش";
          }else if ($result>345600 && $result<=432000) {
              return "4 روز پیش";
          }else if ($result>432000 && $result<=518400) {
              return "5 روز پیش";
          }else if ($result>518400 && $result<=604800) {
              return "6 روز پیش";
          }else if ($result<=604800 && $result<=1209600) {
              return "1 هفته پیش";
          }else if ($result<=1209600 &&  $result<=1814400) {
              return "2 هفته پیش";
          }else if ($result>1814400 && $result<=2419200) {
              return "3 هفته پیش";
          }else if ($result>2419200 && $result<=3024000) {
              return "4 هفته پیش";
          }else if ($result>3024000 && $result<=5184000) {
              return "1 الی 2 ماه پیش";
          }else if ($result>5184000 && $result<=15552000) {
              return "2 الی 6 ماه پیش";
          }else if ($result>15552000 && $result<=31104000) {
              return "6 الی 1 سال پیش";
          }else {
              return "بیش از یک سال پیش";
          }



       }

}
