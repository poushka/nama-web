<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CategoryPrice extends Model
{
    //
    protected $table="category_price";
    public $timestamps=false;
    protected $primaryKey = 'pc_id';

    protected $fillable = [
        'cat_id', 'price1_requierd', 'price2_requierd','price1_title','price2_title'
    ];


//    public function province()
//    {
//        return $this->belongsTo(Province::class);
//    }
//
//    public function districts()
//    {
//        return $this->hasMany(District::class);
//    }


}
