<?php


namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;


class Options_Cat extends Model
{
    protected $table = 'options_cat';

    protected $fillable=['cat_id','option_id'];

    protected $primaryKey = ['cat_id', 'option_id'];

    public $incrementing = false;

    public $timestamps = false;

}
