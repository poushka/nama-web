<?php

namespace App\Http\Controllers;


use App\Admin;
use App\Models\Admin\Agahi;
use App\Models\Admin\Agahi_Emkanat;
use App\Models\Admin\Agahi_Values;
use App\Models\Admin\CategoryPrice;
use App\Models\Admin\Emkanat;
use App\Models\Admin\Options;
use App\Models\Modeling;
use App\Models\User\Agahi_image;
use App\Models\User\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AgahiController extends Controller
{
    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }


    function makeAgahi(Request $request,$cat_id) {
        global $admin_id;
        $rules = [
            'title' => 'required|string|max:255',
            'province_id'=>'required|int',
            'city_id'=>'required|int',
            'lat'=>'required',
            'lang'=>'required',
            'description'=>'required|string'
        ];
       $images = $request->get('images');   //"images":[{"ai_id":1,"name":"20-07-13/1594674813.jpeg"}]
       $emkanats= $request->get('emkanat'); // array liske [{"id":1,"id":2}]


        //we get first pic name atleast there is one image and set it for agahi otherwise pic will be null
        $pic=null;
        if (count($images)>0) {
            $pic=$images[0]['name'];
        }
        //first we add dynamic options to required
//        $options = Options::where([['cat_id',$cat_id],['required',1],['active',1]])->join('type','options.type_id','=','type.type_id')
//            ->select(['eng','required_text'])->get();

//        $options =  Options::where("required",1)
//            ->whereIn('option_id', function($query) use ($cat_id){
//            $query->select('option_id')
//                ->from('options_cat')
//                ->where('cat_id',$cat_id);
//            })->join('type', 'options.type_id', '=', 'type.type_id')
//            ->select(['eng','required_text'])
//            ->get();

        $options = Options::with('optionvalues')
            ->join('options_cat as oc',function ($join) use ($cat_id) {
                $join->on('oc.option_id','=','options.option_id');
                $join->where('oc.cat_id','=',$cat_id);
            })
            ->join('type', 'options.type_id', '=', 'type.type_id')
            ->select(['eng','required_text','created_at'])
            ->orderBy('created_at','asc')
            ->get();




        foreach ($options as $option) {
            $rules[$option->eng]=$option->required_text;
        }
        //adding price required to rules
        $price= CategoryPrice::where('cat_id',$cat_id)->select(['price1_requierd','price2_requierd'])->first();
        if ($price["price1_requierd"]==1){
            $rules["price1"]='required';
        }
        if ($price["price2_requierd"]==1){
            $rules["price2"]='required';
        }

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        //then we create agahi
        $agahi=  Agahi::create([
            'admin_id' => $admin_id,
            'title' => $request->get('title'),
            'metr'=>$request->get("metr"),
            'pic'=>$pic,
            'cat_id' => $cat_id,
            'province_id'=>$request->get('province_id'),
            'city_id'=>$request->get('city_id'),
            'district_id'=>$request->exists('district_id') ?$request->get('district_id'):null ,
            'lat'=>$request->get('lat'),
            'lang'=>$request->get('lang'),
            'price1'=>$price["price1_requierd"]==1?$request->get("price1"):null,
            'price2'=>$price["price2_requierd"]==1?$request->get("price2"):null,
            'description'=>$request->get('description')
        ]);


        //then we insert all options if exists
//        $options = Options::where([['cat_id',$cat_id],['active',1]])->join('type','options.type_id','=','type.type_id')
//            ->select(['option_id', 'eng','type','table_name','column_name'])->get();

//        $options =  Options::whereIn ('option_id', function($query) use ($cat_id){
//                $query->select('option_id')
//                    ->from('options_cat')
//                    ->where('cat_id',$cat_id);
//            })->join('type', 'options.type_id', '=', 'type.type_id')
//            ->select(['option_id', 'eng','type','table_name','column_name'])
//            ->get();


        $options = Options::with('optionvalues')
            ->join('options_cat as oc',function ($join) use ($cat_id) {
                $join->on('oc.option_id','=','options.option_id');
                $join->where('oc.cat_id','=',$cat_id);
            })
            ->join('type', 'options.type_id', '=', 'type.type_id')
            ->select(['options.option_id', 'eng','type','table_name','column_name','created_at'])
            ->orderBy('created_at','asc')
            ->get();




        foreach ($options as $option) {
            if ($request->exists($option->eng)) {

//               $data= ['agahi_id' => $agahi->agahi_id, 'option_id' => $option->option_id,'value'=>$request->get($option->eng)];
//               DB::table($option->table_name)->insert($data);
                Agahi_Values::create([
                    'agahi_id' => $agahi->agahi_id, 'option_id' => $option->option_id,$option->column_name=>$request->get($option->eng)
                ]);
            }
        }
        //update images for agahi
        if (count($images)>0) {
            //first get the list of ids
            $ai_ids = array_column($images, 'ai_id');
            Agahi_image::whereIn('ai_id', $ai_ids)->update(['agahi_id'=>$agahi->agahi_id]);
        }


        if (count($emkanats)>0) {
            $agahi_emkanat = array();
            foreach ($emkanats as $emkanat) {
                array_push($agahi_emkanat,["agahi_id"=>$agahi->agahi_id,'emkanat_id'=>$emkanat['id']]);
            }
            Agahi_Emkanat::insert($agahi_emkanat);
        }

        return $this->successReport($agahi,"آگهی با موفقیت ثبت گردید",201);


    }

    function getCategoryNeeds(Request $request,$cat_id) {

//        $productCategory = Options::with('optionvalues')->where([['cat_id', $cat_id],['active','=',1]])
//            ->join('type', 'options.type_id', '=', 'type.type_id')
//            ->get();

//        $options =  Options::with('optionvalues')->whereIn('option_id', function($query) use ($cat_id){
//            $query->select('option_id')
//                ->from('options_cat')
//                ->where('cat_id',$cat_id)
//                ->orderBy('created_at','asc');
//        })->join('type', 'options.type_id', '=', 'type.type_id')
//            ->orderBy('option_id','asc')
//            ->get();

//        $modeling = Modeling::leftjoin('modeling_like as m',function($join) use ($user_id){
//            $join->on("modeling.modeling_id","=","m.modeling_id");
//            $join->where("m.user_id","=",$user_id);
//
//        })

        $options = Options::with('optionvalues')
            ->join('options_cat as oc',function ($join) use ($cat_id) {
                $join->on('oc.option_id','=','options.option_id');
                $join->where('oc.cat_id','=',$cat_id);
            })
            ->join('type', 'options.type_id', '=', 'type.type_id')
            ->orderBy('created_at','asc')
            ->get();

        $price= DB::table('category_price')->where('cat_id',$cat_id)->first();
        $province = Province::with('cities.districts')->get();
    //    $emkanat = Emkanat::where('cat_id',$cat_id)->get();

        $emkanat =  Emkanat::whereIn('id', function($query) use ($cat_id){
            $query->select('emkanat_id')
                ->from('emkanat_cat')
                ->where('cat_id',$cat_id);
        })->get();


        $mterSuggest = DB::table('suggest_metr')
            ->get();
        $priceSuggest = DB::table('category_price_suggest')->where("cat_id",$cat_id)
            ->get();


        $data=array();
        $data["price"]=$price;
        $data["options"]=$options;
        $data["province"]=$province;
        $data["emkanat"]=$emkanat;
        $data["metr_suggest"]=$mterSuggest;
        $data["price_suggest"]=$priceSuggest;

        return $this->successReport($data,"دریافت موفقیت آمیز",200);

    }



}
