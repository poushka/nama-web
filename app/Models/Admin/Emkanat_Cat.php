<?php


namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;


class Emkanat_Cat extends Model
{
    protected $table = 'emkanat_cat';

    protected $fillable=['cat_id','emkanat_id'];

    protected $primaryKey = ['cat_id', 'emkanat_id'];

    public $incrementing = false;

    public $timestamps = false;

}
