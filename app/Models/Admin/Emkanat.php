<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Emkanat extends Model
{


    //
    protected $table="emkanat";
    public $timestamps=false;


    protected $fillable = [
        'title','icon','active'
    ];


}
