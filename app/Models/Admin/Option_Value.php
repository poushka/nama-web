<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Option_Value extends Model
{


    //
    protected $table="options_values";
    public $timestamps=false;
    protected $primaryKey = 'ov_id';


    protected $fillable = [
        'option_id','value'
    ];

    protected $dates = ['deleted_at'];

    public function options()
    {
        return $this->belongsTo(Options::class);
    }


//    public function province()
//    {
//        return $this->belongsTo(Province::class);
//    }
//
//    public function districts()
//    {
//        return $this->hasMany(District::class);
//    }


}
