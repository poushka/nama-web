<?php


namespace App\Models\Admin;

use App\Models\User\Agahi_image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agahi_Values extends Model
{


    //
    protected $table = 'agahi_values';

    protected $fillable=['agahi_id','option_id','value','int_value'];

    protected $primaryKey = 'id';

    public $timestamps = false;


    public function option() {
        return $this->hasOne(Options::class, 'option_id','option_id')->select('option_id','title','price_type','type_id');

    }



}
