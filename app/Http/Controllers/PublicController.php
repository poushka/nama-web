<?php

namespace App\Http\Controllers;
use App\Models\Modeling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PublicController extends Controller
{


  function getSearchSuggests(Request $request) {
      $searchSuggest = DB::table('search_suggest')
          ->orderBy('ss_id','desc')
          ->get();
          return $this->successReport($searchSuggest,"ok",200);

  }
  function mainFilterNeed(Request $request) {
      $mterSuggest = DB::table('suggest_metr')
          ->get();
      $priceSuggest = DB::table('category_price_suggest')->where("cat_id",1)
          ->get();
      $response = array();
      $response["metr_suggest"]=$mterSuggest;
      $response["price_suggest"]=$priceSuggest;
      return  $this->successReport($response,"ok",200);

  }



}
