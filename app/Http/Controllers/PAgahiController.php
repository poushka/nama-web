<?php

namespace App\Http\Controllers;


use App\Admin;
use App\City;
use App\helper\TimeHelperClass;
use App\Models\Admin\Agahi;
use App\Models\Admin\Agahi_Emkanat;
use App\Models\Admin\CategoryPrice;
use App\Models\Admin\Emkanat;
use App\Models\Admin\Options;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Models\User\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PAgahiController extends Controller
{
    function getAgahi(Request $request,$agahi_id) {
        $result = Agahi::with('images','values','province','district','city')
            ->join('category_price as cp' , 'agahi.cat_id','=','cp.cat_id')
            ->join('category as c','agahi.cat_id','=','c.cat_id')
            ->where('agahi_id',$agahi_id)
            ->select('agahi.*','cp.price1_title','cp.price2_title','c.name as cat_name','c.show_pm')
            ->first();


        $emkanat =  Emkanat::whereIn('id', function($query) use ($agahi_id){
            $query->select('emkanat_id')
                ->from('agahi_emkanat')
                ->where('agahi_id',$agahi_id);
        })->get();
        $result["emkanat"]=$emkanat;
        $result["persianTime"]=TimeHelperClass::getTime($result["created_at"]);
        return $this->successReport($result,"ok",200);

    }

    function getAgahies(Request $request)
    {

       //$page = $request->get('page');
        $order_by='agahi_id'; //optional
        $sort = 'desc'; //optional

        //sort
        if ($request->exists('order')) {
            $order_by=$request->get('order');
        }

        if ($request->exists('sort')) {
            $sort=$request->get('sort');
        }

        $agahies = Agahi::with('province','city','district')
            ->join('category_price as cp','agahi.cat_id','=','cp.cat_id')
           ->join('category as c','agahi.cat_id','=','c.cat_id')
            ->select("agahi.agahi_id","agahi.province_id","agahi.city_id", "agahi.district_id", "agahi.title","agahi.metr",
                "agahi.pic",   "agahi.lat",   "agahi.lang", "agahi.price1",  "agahi.price2","agahi.created_at"
               ,'c.name as cat_name','c.pic as cat_icon' ,'cp.price1_title','cp.price2_title')
            ->orderBy($order_by,$sort);

        //location_filter

     if ($request->exists('district_ids')) {
        $districts  = json_decode($request->get('district_ids',true));
        $agahies->whereIn('district_id',$districts);
    }
     else if ($request->exists('city_id')){
            $agahies->where('city_id',$request->get('city_id'));
        }else  if ($request->exists('province_id')) {
           $agahies->where('province_id',$request->get('province_id'));
       }

        if ($request->exists('cat_ids')) {
            $categories  = json_decode($request->get('cat_ids',true));
            $agahies->whereIn('agahi.cat_id',$categories);
        }




        if ($request->exists('metr_min')) {
            $agahies->where('metr','>=',$request->get('metr_min'));
        }

        if ($request->exists('metr_max')) {
            $agahies->where('metr','<=',$request->get('metr_max'));
        }


        if ($request->exists('price_min')) {
            $agahies->where('price1','>=',$request->get('price_min'));
        }

        if ($request->exists('price_max')) {
            $agahies->where('price1','<=',$request->get('price_max'));
        }
        $agahies = $agahies->paginate(10);

        foreach ($agahies as $agahy) {
            $agahy["persianTime"]=TimeHelperClass::getTime($agahy["created_at"]);
        }
        $cat_loaded = $request->get('cat_loaded');



        if ($request->exists("load_cat")) {
            $categories=Category::where('active',1)->get();
            $agahies = $agahies->toarray();
            $agahies["categories"] =$categories;
        }



      return $this->successReport($agahies,"ok",200);
    }

    function getAgahiByCatId(Request $request,$cat_id)  {




         if ($request->exists('filters')) {

              //   http://localhost/nama/public/api/getAgahiesBycat/1?filters=[{"title":"متراژ","eng":"baghmetr","column_name":"int_value","value":"110","range":true,"max":"120"},
             //  {"title":"سند","eng":"baghsanad","column_name":"value","value":"قولنامه ای","range":false}]

            $filters= json_decode($request->get('filters'),true) ;



             $order_by='agahi_id'; //optional
             $sort = 'desc'; //optional

             //sort
             if ($request->exists('order')) {
                 $order_by=$request->get('order');
             }

             if ($request->exists('sort')) {
                 $sort=$request->get('sort');
             }
             $raw = "";
             $filter_count = count($filters);

             for ($i=0 ; $i<$filter_count; $i++) {
                 $filter = $filters[$i];
                 if ($i+1==$filter_count) {
                     $raw.="max(case WHEN o.title='$filter[title]' THEN v.$filter[column_name] end) as '$filter[eng]'";
                 }else {
                     $raw.="max(case WHEN o.title='$filter[title]' THEN v.$filter[column_name] end) as '$filter[eng]',";
                 }
             }

             $agahies = Agahi::join('category_price as cp','agahi.cat_id','=','cp.cat_id')
                 ->where('agahi.cat_id',$cat_id)
                 ->with('province','city','district')
                 ->join('category as c','agahi.cat_id','=','c.cat_id')
                 ->join('agahi_values as v','agahi.agahi_id','=','v.agahi_id')
                 ->join('options as o','v.option_id','=','o.option_id')
                 ->select('agahi.*' ,'cp.price1_title','cp.price2_title','c.name as cat_name','c.pic as cat_pic',DB::raw($raw))->orderBy($order_by,$sort)
                 ->groupBy('agahi.agahi_id');

             //location_filter

             if ($request->exists('district_ids')) {
                 $districts  = json_decode($request->get('district_ids',true));
                 $agahies->whereIn('district_id',$districts);
             }
              else if ($request->exists('province_id')) {
                 $agahies->where('province_id',$request->get('province_id'));
             }else if ($request->exists('city_id')){
                 $agahies->where('city_id',$request->get('city_id'));
             }

             //price_filter
             if ($request->exists('price_min')) {
                 $agahies->where('price1','>=',$request->get('price_min'));
             }

             if ($request->exists('price_max')) {
                 $agahies->where('price1','<=',$request->get('price_max'));
             }

             if ($request->exists('price2_min')) {
                 $agahies->where('price2','>=',$request->get('price2_min'));
             }

             if ($request->exists('price2_max')) {
                 $agahies->where('price2','<=',$request->get('price2_max'));
             }

             //mter_filter
             if ($request->exists('metr_min')) {
                 $agahies->where('metr','>=',$request->get('metr_min'));
             }

             if ($request->exists('metr_max')) {
                 $agahies->where('metr','<=',$request->get('metr_max'));
             }

             foreach ($filters as $filter) {
                 if ($filter["range"]) {
                     if (isset($filter["value"])) {
                         $agahies->having($filter['eng'],'>=',$filter["value"]);
                     }
                     if (isset($filter["max"])) {
                         $agahies->having($filter['eng'],'<=',$filter["max"]);
                     }
                 }else {
                     $agahies->having($filter["eng"],$filter["value"]);
                 }

             }


             return $this->successReport($agahies->paginate(10),"ok",200);


         }else {
             $order_by='agahi_id'; //optional
             $sort = 'desc'; //optional

             //sort
             if ($request->exists('order')) {
                 $order_by=$request->get('order');
             }

             if ($request->exists('sort')) {
                 $sort=$request->get('sort');
             }


             $agahies = Agahi::join('category_price as cp','agahi.cat_id','=','cp.cat_id')
                 ->with('province','city','district')
                 ->where('agahi.cat_id',$cat_id)
                 ->join('category as c','agahi.cat_id','=','c.cat_id')
                 ->select('agahi.*' ,'cp.price1_title','cp.price2_title','c.name as cat_name','c.pic as cat_pic')->orderBy($order_by,$sort);

             //location_filter
             if ($request->exists('district_ids')) {
                 $districts  = json_decode($request->get('district_ids',true));
                 $agahies->whereIn('district_id',$districts);
             } else if($request->exists('province_id')) {
                 $agahies->where('province_id',$request->get('province_id'));
             }else if ($request->exists('city_id')){
                 $agahies->where('city_id',$request->get('city_id'));
             }

             //price_filter
             if ($request->exists('price_min')) {
                 $agahies->where('price1','>=',$request->get('price_min'));
             }

             if ($request->exists('price_max')) {
                 $agahies->where('price1','<=',$request->get('price_max'));
             }

             if ($request->exists('price2_min')) {
                 $agahies->where('price2','>=',$request->get('price2_min'));
             }

             if ($request->exists('price2_max')) {
                 $agahies->where('price2','<=',$request->get('price2_max'));
             }

             //metr filter

             if ($request->exists('metr_min')) {
                 $agahies->where('metr','>=',$request->get('metr_min'));
             }

             if ($request->exists('metr_max')) {
                 $agahies->where('metr','<=',$request->get('metr_max'));
             }


             return $this->successReport($agahies->paginate(10),"ok",200);
         }


    }

    function search(Request $request) {
        $query = $request->get('query');
        $query=$query."*";
        $agahies = Agahi::whereRaw("MATCH(title,description) AGAINST(? IN BOOLEAN MODE)",[$query])
            ->join('category_price as cp','agahi.cat_id','=','cp.cat_id')
            ->join('category as c' , 'agahi.cat_id','=','c.cat_id')
            ->with('province','city','district')
            ->orderBy('agahi_id','desc')
            ->select("agahi.agahi_id","agahi.province_id","agahi.city_id", "agahi.district_id", "agahi.metr", "agahi.title",  "agahi.pic",   "agahi.lat",
                "agahi.lang", "agahi.price1",  "agahi.price2","agahi.created_at","cp.price1_title","cp.price2_title",'c.name as cat_name');

                 if ($request->exists('district_ids')) {
                     $districts  = json_decode($request->get('district_ids',true));
                     $agahies->whereIn('district_id',$districts);
                 }
                 else if ($request->exists('city_id')){
                     $agahies->where('city_id',$request->get('city_id'));
                 }else  if ($request->exists('province_id')) {
                     $agahies->where('province_id',$request->get('province_id'));
                 }


        return $this->successReport($agahies->paginate(4),"ok",200);
    }


}
