<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{


    //
    protected $table="type";
    public $timestamps=false;
    protected $primaryKey = 'type_id';

    protected $fillable = [
        'type'
    ];


    public function Option()
    {
        return $this->belongsTo(Options::class);
    }
}
