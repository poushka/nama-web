<?php

namespace App\Http\Controllers;


use App\Admin;
use App\helper\TimeHelperClass;
use App\Models\Admin\Agahi;
use App\Models\Admin\Suggest;
use App\Models\Admin\Suggest_Agahi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SuggestController extends Controller
{
    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

    function makeSuggest(Request $request) {
        global $admin_id;
        $rules = [
            'name' => 'required|string|max:255',
            'pic'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048'

        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $pic = $request->pic;

        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $imageName = time().$s. '.'.$pic->extension();
        $directory_pic = "images/suggest/";
        $result= $pic->move(public_path($directory_pic), $imageName);
        $result = Suggest::create(['name'=>$request->get('name'),'pic'=>$imageName]);


        return $this->successReport($result,"لیست جدید با موفقیت ثبت گردید",201);


    }
    function updateSuggest(Request $request,Suggest $suggest) {
        $image_temp_name = $suggest->pic;
        $rules = [
            'name' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        if ($request->exists('pic')) {
            $pic = $request->pic;
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $imageName = time().$s. '.'.$pic->extension();
            $directory_pic = "images/suggest/";
            $result= $pic->move(public_path($directory_pic), $imageName);
        }
        $suggest->update(['name'=>$request->get('name'),'pic'=>
        $request->exists('pic') ? $imageName : $suggest->pic
        ]);
        if ($suggest->wasChanged()) {
            @unlink($directory_pic.$image_temp_name);
            return $this->successReport($suggest,"به روز رسانی لیست انجام شد",201);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }



    }
    function deleteSuggest(Request $request,Suggest $suggest) {
        $address="images/suggest/";
        @unlink($address.$suggest->pic);
        $suggest->delete();
        return response()->json([],204);
    }
    function getSuggestList(Request $request) {

        $result = Suggest::leftJoin('suggest_agahi as a','suggest.suggest_id','=','a.suggest_id')
            ->select('suggest.*',DB::raw('count(a.agahi_id) as total'))
            ->groupBy('suggest.suggest_id')
            ->get();
        return $this->successReport($result,"ok",200);
    }


    function addAgahiSuggest(Request $request) {
        $rules = [
            'suggest_id' => 'required|int',
            'agahi_id'=>'required|int'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }


        try {
            $result = Suggest_Agahi::create([
                'agahi_id'=>$request->get('agahi_id'),
                'suggest_id'=>$request->get('suggest_id')
            ]);

            return $this->successReport($result,"آگهی جدید به لیست اضافه شد",201);
        }catch (\Exception $e) {
            return  $this->failureResponse("خطا در ثبت",400);
        }


    }

    function deleteAgahiSuggest(Request $request,$suggest_id,$agahi_id) {
        $agahi_suggest = Suggest_Agahi::where([['agahi_id',$agahi_id],
            ['suggest_id',$suggest_id]
            ])->delete();

        if ($agahi_suggest > 0) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در حذف",200);
        }

    }



    function getAgahiSuggestion(Request $request , $suggest_id) {
        $suggeest_agaies   =   Suggest_Agahi::where('suggest_id',$suggest_id)->get();
        $agahi_ids = array_column($suggeest_agaies->toArray(), 'agahi_id');
        $agahies = Agahi::with('province','city','district')->whereIn('agahi_id',$agahi_ids)
            ->join('category_price as cp','agahi.cat_id','=','cp.cat_id')
            ->join('category as c','agahi.cat_id','=','c.cat_id')
            ->select("agahi.agahi_id","agahi.province_id","agahi.city_id", "agahi.district_id", "agahi.title","agahi.metr",
                "agahi.pic",   "agahi.lat",   "agahi.lang", "agahi.price1",  "agahi.price2","agahi.created_at"
                ,'c.name as cat_name','c.pic as cat_icon' ,'cp.price1_title','cp.price2_title')
            ->get();
        foreach ($agahies as $agahy) {
            $agahy["persianTime"]=TimeHelperClass::getTime($agahy["created_at"]);
        }
        return $this->successReport($agahies,"ok",200);
    }

}
