<?php


namespace App\Models\Admin;

use App\Models\User\Agahi_image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agahi_Emkanat extends Model
{


    //
    protected $table = 'agahi_emkanat';

    protected $fillable=['agahi_id,emkanat_id'];

    protected $primaryKey = ['agahi_id', 'emkanat_id'];

    public $incrementing = false;

    public $timestamps = false;

    public function emkan() {
        return $this->hasOne(Emkanat::class,'id','emkanat_id');
    }


}
