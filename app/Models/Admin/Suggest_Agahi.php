<?php


namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;


class Suggest_Agahi extends Model
{
    protected $table = 'suggest_agahi';

    protected $fillable=['agahi_id','suggest_id'];

    protected $primaryKey = ['agahi_id', 'suggest_id'];

    public $incrementing = false;

    public $timestamps = false;

}
