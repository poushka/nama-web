<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Admin_Privilage;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminController extends Controller
{

    //registration
    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);

    }
    function register(Request $request) {


        $rules = [
            'username' => 'required|string|max:255|unique:admin',
            'phone' => 'required',
            'password'=> 'required|min:6',
            'name'=>'required',
            'family'=>'required',
            'pic' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ];
        $validator_message=['username.unique'=>"این نام کاربری قبلا ثبت شده است",
            'password.min'=>'رمز عبور باید حداقل 6 کاراکتر باشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }


        $pic = null ;
        if ($request->hasFile('pic')) {
            $my_image = $request->file('pic');
            $date_folder = date("y-m-d");
            $imageName = time().'.'.$request->pic->extension();
            $directory =  "images/admin/thumb/$date_folder";
            if (!File::exists(public_path().'/'.$directory)) {
                File::makeDirectory(public_path().'/'.$directory,0777,true);
            }
            Image::make($my_image)->resize(300, 300,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path("images/admin/thumb/$date_folder/" . $imageName));
            $request->pic->move(public_path("images/admin/pic/$date_folder"), $imageName);
            $pic=$date_folder."/".$imageName;

        }


            $admin=  Admin::create([
                'username' => $request->get('username'),
                'phone' => $request->get('phone'),
                'password' => bcrypt($request->get('password')),
                 'name'=>$request->get('name'),
                'family'=>$request->get('family'),
                'pic'=>$pic
            ]);

            $token = JWTAuth::fromUser($admin);
            $admin["token"]=$token;
            return $this->successReport($admin,"ثبت نام با موفیت انجام گردید",201);

    }
    function login(Request $request) {
        $rules = [
            'username' => 'required',
            'password'=> 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $credentials = $request->only("username", "password");
        if (!$token = JWTAuth::attempt($credentials)) {
          return $this->failureResponse("رمز عبور یا نام کاربری صحیح نمیباشد",401);

        }else{
            $currentUser = Auth::user();
            $data = [
                'access_token' => $token,
                'token_type' => 'bearer',
                 'admin'=>$currentUser
            ];

            return $this->successReport($data,"ورود با موفقیت انجام شد",200);
        }

    }

    function getAdmins() {
        $admins = Admin::all();
        return $this->successReport($admins,"",200);
    }

    function adminPrivilages($id) {

        $privilages = Privilage::all();
        $admins = Admin_Privilage::where('admin_id',$id)->get();
        foreach ($privilages as $privilage) {
              $privilage['has_priv']=false;
              foreach ($admins as $admin) {
                  if ($privilage->id==$admin->privilage_id) {
                      $privilage["has_priv"]=true;
                      break;
                  }
              }
        }

      return $this->successReport($privilages,"دریافت موفقیت آمیز",200);




    }



    function addPrivilage(Request $request) {

        $rules = [
            'admin_id' => 'required',
            'privilage_id' => 'required',

        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $priv = new Admin_Privilage();
        $priv->admin_id = $request->admin_id;
        $priv->privilage_id=$request->privilage_id;
        try {
             $priv->save();
            return $this->successReport($priv,"دسترسی با موفقیت ایجاد شد",201);

        }catch(\Exception $e) {
      return   $this->failureResponse("خطا در ایجاد درسترسی",400);
        }




    }

    function deletePrivilage(Request $request,$admin_id,$privilage_id) {

         $priv = Admin_Privilage::where([['admin_id',$admin_id],['privilage_id',$privilage_id]])->delete();
         if ($priv>0) {
             return response()->json("",204);
         }else {
             return   $this->failureResponse("خطا در حذف درسترسی",400);
         }




    }



    public function getAuthenticatedUser()
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                //  if (! $user = auth('api')->parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }








}
