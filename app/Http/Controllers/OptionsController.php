<?php

namespace App\Http\Controllers;
use App\Admin;
use App\Models\Admin\Emkanat;
use App\Models\Admin\Emkanat_Cat;
use App\Models\Admin\Option_Value;
use App\Models\Admin\Options;
use App\Models\Admin\Options_Cat;
use App\Models\Admin\OptionValues;
use App\Models\Admin\Suggest_Agahi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OptionsController extends Controller
{

    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }




    function getOptionsAll(Request $request) {
        $options =  Options::with('optionvalues')->join('type', 'options.type_id', '=', 'type.type_id')
            ->get();
        return $this->successReport($options, "دریافت با موفقیت انجام شد", 200);
    }


    function getOptions(Request $request,$cat_id) {
        $options =  Options::with('optionvalues')->whereIn('option_id', function($query) use ($cat_id){
            $query->select('option_id')
                ->from('options_cat')
                ->where('cat_id',$cat_id);
        })->join('type', 'options.type_id', '=', 'type.type_id')
            ->get();
        return $this->successReport($options, "دریافت با موفقیت انجام شد", 200);
    }

    function deleteOption(Request $request,$id) {

        $option = Options::find($id);
        if (is_null($option)) {
            return $this->failureResponse("آیتم مورد نظر یافت نشد",400);
        }
        $option->delete();
        return response()->json([],204);

    }

    function makeOption(Request $request) {
      $rules = [
          'title' => 'required',
          'eng' => 'required',
          'price_type'=>'required|int|max:1|min:0',
          'required'=> 'required|int|max:1|min:0',
          'type_id'=> 'required',
      ];
      //"option_values":["test","test1"] a list of values is Optional

      $validator = Validator::make($request->all(),$rules);
      if ($validator->fails()) {
          return $this->failureResponse($validator->errors()->first(),422);
      }
          $insert = Options::create($request->all());
          $values_list =$request->get('option_values');

      if(count($values_list)>0) {
          $insert_array_value = array();
          foreach ($values_list as $value){
              $temp_value = ['option_id'=>$insert->option_id,'value'=>$value];
              array_push($insert_array_value,$temp_value);
          }
          Option_Value::insert($insert_array_value);
      }


          if (!$insert) {
              return $this->failureResponse("خطا در ذخیره", 400);
          }

        $productCategory = Options::with('optionvalues')->where('option_id', $insert->option_id)->join('type', 'options.type_id', '=', 'type.type_id')
            ->first();

          return $this->successReport($productCategory, "ویژگی جدید اضافه شد", 201);
  }

    function updateOption(Request $request,Options $option) {
        $rules = [
            'title' => 'required',
            'eng' => 'required',
            'price_type'=>'required|int|max:1|min:0',
            'required'=> 'required|int|max:1|min:0',
            'type_id'=> 'required',
        ];
        //"option_values":["test","test1"] a list of values is Optional

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $option->update($request->all());
        if ($option->wasChanged()) {
            $values_list =$request->get('option_values');
            if(count($values_list)>0) {
                Option_Value::where('option_id', $option->option_id)->delete();
                $insert_array_value = array();
                foreach ($values_list as $value){
                    $temp_value = ['option_id'=>$option->option_id,'value'=>$value];
                    array_push($insert_array_value,$temp_value);
                }
                Option_Value::insert($insert_array_value);
                return $this->successReport([],"",204);
            }else {
                return $this->successReport([],"",204);
            }

        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }










        $productCategory = Options::with('optionvalues')->where('option_id', $insert->option_id)->join('type', 'options.type_id', '=', 'type.type_id')
            ->first();

        return $this->successReport($productCategory, "ویژگی جدید اضافه شد", 201);


    }



    function activateOption(Request $request,Options $options) {
        $rules = [
            'active' => 'required|int|min:0|max:1'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $options->update($request->all());
        if ($options->wasChanged()) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }

    function renameOptions(Request $request,Options $options) {
        $rules = [
            'title' => 'required|min:3'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $options->update($request->all());
        if ($options->wasChanged()) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }






    //option cat functions

    function addOptionCat(Request $request) {
        $rules = [
            'cat_id' => 'required|int',
            'option_id'=>'required|int'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }


        try {
            $result = Options_Cat::create([
                'cat_id'=>$request->get('cat_id'),
                'option_id'=>$request->get('option_id')
            ]);

            return $this->successReport($result,"ویژگی جدید  به دسته اضافه شد",201);
        }catch (\Exception $e) {
            return  $this->failureResponse("خطا در ثبت",400);
        }


    }
    function deleteOptinCat(Request $request,$cat_id,$option_id) {

        $result = Options_Cat::where([['cat_id',$cat_id],
            ['option_id',$option_id]
        ])->delete();

        if ($result > 0) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در حذف",200);
        }

    }



    //option-values
    function addOptionValue(Request $request) {
        $rules = [
            'option_id' => 'required',
            'value' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $insert = Option_Value::create($request->all());
        if (!$insert) {
            return $this->failureResponse("خطا در ذخیره", 400);
        }
        return $this->successReport($insert, "ویژگی جدید اضافه شد", 201);
    }

    function deleteOptionValue(Request $request,$id) {

        $option_value = Option_Value::find($id);
        if (is_null($option_value)) {
            return $this->failureResponse("آیتم مورد نظر یافت نشد",400);
        }
        $option_value->delete();
        return response()->json([],204);

    }
    function getTypes() {
        $types = DB::table('type')->get();
        return $this->successReport($types,"موفقیت آمیز",200);
    }





    //emkanat
    function getAllEmkanat() {
        $result = Emkanat::get();
        return $this->successReport($result, "دریافت با موفقیت انجام شد", 200);
    }

    function getEmkanat(Request $request ,$id) {
         $emkanat =  Emkanat::whereIn('id', function($query) use ($id){
            $query->select('emkanat_id')
                ->from('emkanat_cat')
                ->where('cat_id',$id);
        })->get();
        return $this->successReport($emkanat, "دریافت با موفقیت انجام شد", 200);
    }


    function makeEmkanat(Request $request) {
        $rules = [
            'title' => 'required',
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $pic = $request->pic;
        $imageName = time().'.'.$pic->extension();
        $directory_pic =  "images/emkanat/";
        $result= $pic->move(public_path($directory_pic), $imageName);
        $emakant = Emkanat::create(['title'=>$request->get('title'),
                                       'icon'=>$imageName]);

        if (!$emakant) {
            return $this->failureResponse("خطا در ذخیره", 400);
        }
        return $this->successReport($emakant, "امکان جدید اضافه شد", 201);

    }

    function updateEmkanat(Request $request , Emkanat $emkanat) {
        $image_temp_name = $emkanat->icon;
        $rules = [
            'title' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        if ($request->exists('pic')) {
            $pic = $request->pic;
            $imageName = time().'.'.$pic->extension();
            $directory_pic =  "images/emkanat/";
            $pic->move(public_path($directory_pic), $imageName);
        }


        $emkanat->update(['title'=>$request->get('title'),'icon'=>
            $request->exists('pic') ? $imageName : $emkanat->icon
        ]);

        if ($emkanat->wasChanged()) {
            @unlink($directory_pic.$image_temp_name);
            return $this->successReport($emkanat,"به روز رسانی امکان انجام شد",201);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }



    function deleteEmkanat(Request $request ,Emkanat $emkanat) {
        $emkanat->delete();
        return response()->json([],204);

    }
    function emkanatActivation(Request $request ,Emkanat $emkanat) {
        $rules = ["active"=>"required|int|min:0|max:1"];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $request = $request->only('active');
        $emkanat->update($request);
        if ($emkanat->wasChanged()) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }

    function addEmkanCat(Request $request) {
        $rules = [
            'cat_id' => 'required|int',
            'emkanat_id'=>'required|int'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }


        try {
            $result = Emkanat_Cat::create([
                'cat_id'=>$request->get('cat_id'),
                'emkanat_id'=>$request->get('emkanat_id')
            ]);

            return $this->successReport($result,"امکان جدید  به دسته اضافه شد",201);
        }catch (\Exception $e) {
            return  $this->failureResponse("خطا در ثبت",400);
        }


    }

    function deleteEmkanCat(Request $request,$cat_id,$emkan_id) {

        $result = Emkanat_Cat::where([['cat_id',$cat_id],
            ['emkanat_id',$emkan_id]
        ])->delete();

        if ($result > 0) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در حذف",200);
        }

    }





}
