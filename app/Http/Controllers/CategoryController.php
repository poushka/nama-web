<?php

namespace App\Http\Controllers;


use App\Admin;
use App\Http\Controllers\Controller;
use App\Models\User\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CategoryController extends Controller
{

    function getAll() {
        $categories= Category::where('active','=',1)->get();
        return  $this->successReport($categories,"",200);
}


}
