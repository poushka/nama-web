<?php


namespace App\Models\Admin;

use App\City;
use App\District;
use App\Models\User\Agahi_image;
use App\Models\User\Province;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agahi extends Model
{


    //
    protected $table="agahi";
    protected $primaryKey = 'agahi_id';

    protected $fillable = [
      'admin_id','pic', 'title', 'metr', 'cat_id', 'province_id','city_id','district_id','lat'
        ,'lang','price1','price2','description'];


      public function images() {
          return $this->hasMany(Agahi_image::class,'agahi_id')->select('ai_id','name','agahi_id');
      }


    public function values() {
        return $this->hasMany(Agahi_Values::class,'agahi_id','agahi_id')->with('option.type');
    }

    public function province() {
        return $this->hasOne(Province::class,'id','province_id')->select("id","name");
    }

    public function city() {
        return $this->hasOne(City::class,'id','city_id')->select("id","name");
    }

    public function district() {
        return $this->hasOne(District::class,'id','district_id')->select("id","name");
    }


    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->getTimestamp();
    }


}
