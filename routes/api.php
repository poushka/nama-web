<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




Route::group(['prefix'=>"admin"], function() {
    Route::post('login', 'AdminController@login');
    //routes needing authentication
    Route::group(['middleware'=>'adminAuth'],function() {

        Route::post('register', 'AdminController@register');
        Route::get('getAdmins', 'AdminController@getAdmins');
        Route::get('adminPrivilages/{admin_id}', 'AdminController@adminPrivilages');

        Route::post('privilage', 'AdminController@addPrivilage');
        Route::delete('privilage/{admin_id}/{privilage_id}', 'AdminController@deletePrivilage');


        //Category functions
        Route::get('categories', 'CategoryControllerAdmin@getAll');
        Route::post('categories', 'CategoryControllerAdmin@make');
        Route::put('categories/{category}', 'CategoryControllerAdmin@update');
        Route::delete('categories/{category}', 'CategoryControllerAdmin@delete');
        Route::put('categoriesActivate/{category}', 'CategoryControllerAdmin@activation');

        //CategoryPrice functions
        Route::get('catPrice/{cat_id}', 'CatPriceController@getCatPrice');
        Route::post('catPrice', 'CatPriceController@makeCatPrice');
        Route::put('catPrice/{categoryPrice}', 'CatPriceController@updateCatPrice');

        //options functions
        Route::get('getOptionsAll', 'OptionsController@getOptionsAll');
        Route::get('getOptions/{cat_id}', 'OptionsController@getOptions');
        Route::post('makeOption', 'OptionsController@makeOption');
        Route::post('updateOption/{option}', 'OptionsController@updateOption');

        Route::delete('deleteOption/{option_id}', 'OptionsController@deleteOption');

        Route::post('addOptionCat', 'OptionsController@addOptionCat');
        Route::delete('deleteOptionCat/{cat_id}/{option_id}', 'OptionsController@deleteOptinCat');


        Route::put('activateOption/{options}', 'OptionsController@activateOption');
        Route::put('renameOptions/{options}', 'OptionsController@renameOptions');
        Route::get('getTypes', 'OptionsController@getTypes');



        //options value  functions
        Route::post('optionValue', 'OptionsController@addOptionValue');
        Route::delete('optionValue/{ov_id}', 'OptionsController@deleteOptionValue');


        //emkanat   functions
        Route::get('emkanatAll', 'OptionsController@getAllEmkanat');
        Route::get('emkanat/{cat_id}', 'OptionsController@getEmkanat');
        Route::post('emkanat', 'OptionsController@makeEmkanat');
        Route::post('updateEmkan/{emkanat}', 'OptionsController@updateEmkanat');

        Route::delete('emkanat/{emkanat}', 'OptionsController@deleteEmkanat');
        Route::put('activateEmkanat/{emkanat}', 'OptionsController@emkanatActivation');


        Route::post('addEmkanCat', 'OptionsController@addEmkanCat');
        Route::delete('deleteEmkanCat/{cat_id}/{emkan_id}', 'OptionsController@deleteEmkanCat');



       //makeAgahi
        Route::post('agahi/{cat_id}', 'AgahiController@makeAgahi');

        //sugestion
        Route::get('suggest', 'SuggestController@getSuggestList');
        Route::post('suggest', 'SuggestController@makeSuggest');
        Route::post('updateSuggest/{suggest}', 'SuggestController@updateSuggest');
        Route::delete('suggest/{suggest}', 'SuggestController@deleteSuggest');


        Route::get('getAgahiSuggest/{suggest_id}', 'SuggestController@getAgahiSuggestion');
        Route::post('addَAgahiSuggest', 'SuggestController@addAgahiSuggest');
        Route::delete('deleteAgahiSuggest/{suggest_id}/{agahi_id}', 'SuggestController@deleteAgahiSuggest');

    });

});


Route::post('Province', 'ProvinceContoroller@store');
Route::get('Province', 'ProvinceContoroller@getAll');
Route::get('categories', 'CategoryController@getAll');

Route::post('makeImage', 'UploadController@imageUploadPost');
Route::get('catNeeds/{cat_id}', 'AgahiController@getCategoryNeeds');


Route::get('getAgahi/{agahi_id}', 'PAgahiController@getAgahi');
Route::get('getAgahies', 'PAgahiController@getAgahies');
Route::get('getAgahiesBycat/{cat_id}', 'PAgahiController@getAgahiByCatId');

Route::get('search', 'PAgahiController@search');
Route::get('searchSuggests', 'PublicController@getSearchSuggests');

//suggests
Route::get('suggest', 'SuggestPublicController@getSuggestList');
Route::get('getAgahiSuggest/{suggest_id}', 'SuggestPublicController@getAgahiSuggestion');

Route::get('mainFilter', 'PublicController@mainFilterNeed');
