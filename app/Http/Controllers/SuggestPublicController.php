<?php

namespace App\Http\Controllers;
use App\helper\TimeHelperClass;
use App\Models\Admin\Agahi;
use App\Models\Admin\Suggest;
use App\Models\Admin\Suggest_Agahi;
use Illuminate\Http\Request;



class SuggestPublicController extends Controller
{





    function getSuggestList(Request $request) {

        $result = Suggest::get();
        return $this->successReport($result,"ok",200);
    }

    function getAgahiSuggestion(Request $request , $suggest_id) {
        $suggeest_agaies   =   Suggest_Agahi::where('suggest_id',$suggest_id)->get();
        $agahi_ids = array_column($suggeest_agaies->toArray(), 'agahi_id');
        $agahies = Agahi::with('province','city','district')->whereIn('agahi_id',$agahi_ids)
            ->join('category_price as cp','agahi.cat_id','=','cp.cat_id')
            ->join('category as c','agahi.cat_id','=','c.cat_id')
            ->select("agahi.agahi_id","agahi.province_id","agahi.city_id", "agahi.district_id", "agahi.title","agahi.metr",
                "agahi.pic",   "agahi.lat",   "agahi.lang", "agahi.price1",  "agahi.price2","agahi.created_at"
                ,'c.name as cat_name','c.pic as cat_icon' ,'cp.price1_title','cp.price2_title')
            ->get();
        foreach ($agahies as $agahy) {
            $agahy["persianTime"]=TimeHelperClass::getTime($agahy["created_at"]);
        }
        return $this->successReport($agahies,"ok",200);
    }

}
