<?php
namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'category';
    protected $fillable=['name','pic', 'active'];
    protected $primaryKey='cat_id';
    public $timestamps = false;


}