<?php

namespace App\Http\Controllers;


use App\Admin;


use App\Http\Controllers\Controller;
use App\Models\Admin\CategoryPrice;
use App\Models\User\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class CatPriceController extends Controller
{

    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

  function makeCatPrice(Request $request) {
      $rules = [
          'cat_id' => 'required|unique:category_price',
          'price1_requierd' => 'required|int|max:1|min:0',
          'price2_requierd'=> 'required|int|max:1|min:0'
      ];

      $validator = Validator::make($request->all(),$rules);
      if ($validator->fails()) {
          return $this->failureResponse($validator->errors()->first(),422);
      }
          $insert = CategoryPrice::create($request->all());
          if (!$insert) {
              return $this->failureResponse("خطا در ذخیره", 400);
          }
          return $this->successReport([], "نوع قیمت با موفقیت ذخیره شد", 201);
  }


    function updateCatPrice(Request $request,CategoryPrice $categoryPrice) {
        $rules = [
            'price1_requierd' => 'required|int|max:1|min:0',
            'price2_requierd'=> 'required|int|max:1|min:0'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $categoryPrice->update($request->all());
        if ($categoryPrice->wasChanged()) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }
    }


  function getCatPrice($cat_id) {
      $cat_price = CategoryPrice::where('cat_id',$cat_id)->first();
      if ($cat_price==null) {
          return $this->failureResponse("بدون سیاست قیمتی",404);
      }
      return $this->successReport(  $cat_price,'دریافت با موفقیت انجام شد',200);
  }

}
