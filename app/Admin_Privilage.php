<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Admin_Privilage extends Model {

    protected $table = 'admin_privilage';

    protected $fillable=['admin_id,privilage_id'];

    protected $primaryKey = ['admin_id', 'privilage_id'];

    public $incrementing = false;

    public $timestamps = false;




}