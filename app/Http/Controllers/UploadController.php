<?php

namespace App\Http\Controllers;
use App\Admin;
use App\Models\Admin\Option_Value;
use App\Models\Admin\Options;
use App\Models\Admin\OptionValues;
use App\Models\User\Agahi_image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class UploadController extends Controller
{


    public function imageUploadPost(Request $request)
    {
        $rules = [
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'identifier'=>'required|string'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }


        $pic = $request->pic;
        $date_folder = date("y-m-d");
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        $imageName = time().$s. '.'.$pic->extension();
        $directory_thumb =  "images/agahi/thumb/$date_folder/";
        $directory_pic = "images/agahi/pic/$date_folder";

        if (!File::exists(public_path().'/'.$directory_thumb)) {
            File::makeDirectory(public_path().'/'.$directory_thumb,0777,true);
        }

//        if (!File::exists(public_path().'/'.$directory_pic)) {
//            File::makeDirectory(public_path().'/'.$directory_pic,0777,true);
//        }


        $image = Image::make($pic)->resize(300, 300,function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path($directory_thumb . $imageName));
        $result= $pic->move(public_path($directory_pic), $imageName);


        $agahi_image = new Agahi_image();
        $agahi_image->name=$date_folder.'/'.$imageName;
        $agahi_image->save();
        return $this->successReport(['ai_id'=>$agahi_image->ai_id,'identifier'=>$request->identifier,'name'=>$date_folder.'/'.$imageName
        ],"آپلود موفقیت آمیز",200);


    }

}
