<?php


namespace App\Models\Admin;

use App\City;
use App\District;
use App\Models\User\Agahi_image;
use App\Models\User\Province;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suggest extends Model
{



    protected $table="suggest";
    protected $primaryKey = 'suggest_id';
    public $timestamps = false;
    protected $fillable = [
          'name','pic'
        ];





}
