<?php

namespace App\Http\Middleware;

use App\Admin_Privilage;
use App\Privilage;
use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      try {
            global $admin_id;
            $admin = JWTAuth::parseToken()->authenticate();
            $url = $request->segment(count(request()->segments())); //get last part of url
            $privilages = Privilage::where('action_url','LIKE','%'.$url.'%')->get();
            $admin_id = $admin->id;
            //it means that current root need privilate
            if (count($privilages)>0){
                $has_access = Admin_Privilage::where([['admin_id','=',$admin->id],['privilage_id','=',$privilages[0]->id]])->get();

                if ($has_access->isEmpty()) {
                    return response()->json(['message' => 'شما به این بخش دسترسی ندارید','status'=>403],403);
                }
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['message' => 'Token is Invalid','status'=>401],401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['message' => 'Token is Expired','status'=>401],401);
            }else{
                return response()->json(['message' => 'Authorization Token not found','status'=>401],401);
            }
        }
        return $next($request);
    }
}
